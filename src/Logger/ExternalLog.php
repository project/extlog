<?php

namespace Drupal\extlog\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Logs events in a remote log server.
 */
class ExternalLog implements LoggerInterface {

  use RfcLoggerTrait;
  use DependencySerializationTrait;

  /**
   * A configuration object containing extlog settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's place holders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The current environment type.
   *
   * @var string
   */
  protected $environment;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $accountProxy;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a SysLog object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Drupal\Core\Session\AccountProxy $account_proxy
   *   The current user account.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
      ConfigFactoryInterface $config_factory,
      LogMessageParserInterface $parser,
      AccountProxy $account_proxy,
      MessengerInterface $messenger
    ) {
    $this->config = $config_factory->get('extlog.settings');
    $this->parser = $parser;
    $this->environment = isset($_ENV['AH_SITE_ENVIRONMENT']) ? $_ENV['AH_SITE_ENVIRONMENT'] : 'local';
    $this->accountProxy = $account_proxy;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    try {
      if ($this->config->get('active') !== TRUE) {
        if ($this->accountProxy->hasPermission('administer extlog')) {
          $this->messenger->addWarning(
            t('The module extlog is installed but is disabled at <a href="/admin/config/development/logging">configuration</a>.')
          );
        }
        return;
      }
      if (strlen($this->config->get('env.' . $this->environment . '.srv_address')) < 8) {
        if ($this->accountProxy->hasPermission('administer extlog')) {
          $this->messenger->addWarning(
            t('The module extlog is enabled but no server address is active. Please disable the module or define a valid server address.')
          );
        }
        return;
      }

      $operation = $this->getLogOperation($context['channel'], $message, $level);
      if ($operation === FALSE) {
        return;
      }
      $this->sendMessage($level, $message, $context, $operation);
    }
    catch (\Exception $e) {
      return;
    }
  }

  /**
   * Get log operation for a specific log_entry against configured logger.
   *
   * @param string $event
   *   The type of event.
   * @param string $message
   *   The message to log.
   * @param int $severity
   *   Level of the log.
   *
   * @return bool
   *   Returns operation or false.
   */
  private function getLogOperation($event, $message, $severity) {
    $severities = [
      'emergency',
      'alert',
      'critical',
      'error',
      'warning',
      'notice',
      'info',
      'debug',
    ];
    for ($i = 0; $i < $this->config->get('n_rules'); $i++) {
      if (
              $event === $this->config->get('conf.' . $i . '.event') &&
              @preg_match('/' . $this->config->get('conf.' . $i . '.regex') . '/', $message) &&
              $this->config->get('conf.' . $i . '.' . $severities[$severity])
      ) {
        return $this->config->get('conf.' . $i . '.operation');
      }
    }
    return FALSE;
  }

  /**
   * Internal function for parsing a watchdog $log_entry in logger format.
   *
   * @param int $level
   *   Level of the log.
   * @param string $message
   *   The message to log.
   * @param array $context
   *   The context in which the event occurred.
   * @param string $operation
   *   The operation that occurred.
   *
   * @return array
   *   Parsed message in logger format.
   */
  private function parseLogEntry($level, $message, array $context, $operation) {
    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $account_uid = (array_key_exists('uid', $context)) ? $context['uid'] : 0;
    $account_name = $account_uid ? $this->accountProxy->load($account_uid)->getAccountName() : 'ANONYMOUS';    $event_message = [];
    $event_message[] = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);
    $event_message[] = 'Session: ' . session_id();
    $event_message[] = 'User Id: ' . $account_uid;
    $event_message[] = 'User Name: ' . $account_name;
    $event_message[] = isset($context['request_uri']) ? 'Request: ' . $context['request_uri'] : '';
    return [
      'dateTime' => date('Ymd\THis+0000', $context['timestamp']),
      'eventResult' => $level < 4 ? 'NOK' : 'OK',
      'eventType' => $level < 4 ? 'ERROR' : 'INFO',
      'message' => implode(" - ", $event_message),
      'operation' => $operation,
      'session' => session_id() . '|' . $account_uid . '|' . $account_name,
    ];
  }

  /**
   * Send message to the logger server.
   *
   * @param int $level
   *   Level of the log.
   * @param string $message
   *   The message to log.
   * @param array $context
   *   The context in which the event occurred.
   * @param string $operation
   *   The operation that occurred.
   */
  private function sendMessage($level, $message, array $context, $operation) {
    $curl_post_data = http_build_query($this->parseLogEntry($level, $message, $context, $operation));
    $header = [
      $this->config->get('env.' . $this->environment . '.srv_header') . ': ' . $this->config->get('env.' . $this->environment . '.srv_value'),
    ];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $this->config->get('env.' . $this->environment . '.srv_address'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $curl_response = curl_exec($curl);
    curl_close($curl);
  }

}
