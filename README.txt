CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

The External logging (extlog) module monitors your system, 
capturing system events and sends them to a remote log server.


REQUIREMENTS
------------

No requirements


INSTALLATION
------------

    1. Copy external_logging folder to modules directory
    2. At Administration >> Extend, enable the External Logging.


CONFIGURATION
-------------

The configuration page for this module is at:
  Administration >> Configuration >> System >> Logging Settings


MAINTAINERS
-----------

Maintainer: ricardotenreiro (https://www.drupal.org/u/ricardotenreiro)
Maintainer: dgaspara (https://www.drupal.org/u/dgaspara)
Maintainer: joaomarques736 (https://www.drupal.org/u/joaomarques736)
Maintainer: miguelpamferreira (https://www.drupal.org/u/miguelpamferreira)
Maintainer: joao.figueira (https://www.drupal.org/u/joaofigueira)
