<?php

namespace Drupal\Tests\extlog\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\extlog\Logger\ExternalLog;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group extlog
 */
class ExternalLogTest extends UnitTestCase {

  /**
   * Assert instance of ExternalLog can be created.
   */
  public function testCanCreateInstance() {
    $config_factory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $parser = $this->createMock('Drupal\Core\Logger\LogMessageParserInterface');

    $extLog = new ExternalLog($config_factory, $parser);

    $this->assertTrue($extLog instanceof ExternalLog);
  }

  /**
   * Assert log() method returns void.
   *
   * @covers Drupal\extlog\Logger\ExternalLog::log
   */
  public function testLog() {
    // Set get('active') to return true.
    $configMock = $this->createMock('anyClass');
    $configMock->method('get')->willReturn(TRUE);

    $config_factory = $this->createMock('Drupal\Core\Config\ConfigFactoryInterface');
    $config_factory->method('get')->willReturn($configMock);

    $parser = $this->createMock('Drupal\Core\Logger\LogMessageParserInterface');

    $extLog = new ExternalLog($config_factory, $parser);

    $this->assertNull($extLog->log('', ''));
  }

}
